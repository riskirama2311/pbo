package Pertemuan1;

public class Soal2_C{

    public static void main(String[] args) {
        /*
        Rumus Volume Tabung
        V = phi x r kuadrat (d) x t
        */
    
        double phi = 3.14;
        int d = 5;
        int t = 10;
        double V;
        
        V = phi * d * t;
        
        System.out.println("======= MENGHITUNG VOLUME TABUNG =======");
        System.out.println("Volume tabung adalah "+V);
        System.out.println("========================================");
    }
    
}
