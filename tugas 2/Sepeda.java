package sepeda;

public class Sepeda {
    int gear = 5;
    
    public Sepeda(int jumlahRoda,String jenis,String merk){
        System.out.println("Sepeda "+jenis+" bermerk "+merk+" memiliki jumlah roda "+jumlahRoda);
    }
    void rem(){
        System.out.println("Sepeda direm");
    }
    
    public static void main(String[] args) {
        //objek
        Sepeda sepedaFiksi = new Sepeda(2,"fiksi","TDR3000");
        //akses atribut dan method
        int gearSepeda = sepedaFiksi.gear;
        System.out.println(gearSepeda);
        sepedaFiksi.rem();

    }
    
}

