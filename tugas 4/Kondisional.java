import java.util.Scanner;

public class Kondisional{
    public static void main(String[] args){
        Scanner inp = new Scanner(System.in);
        float nilai = inp.nextFloat();
        Nilai nl = new Nilai();

        char huruf = nl.getNilHuruf(nilai);
        String predikat = nl.getPredikat(huruf);

        System.out.println("Nilai dari " + nilai + " dengan Huruf " + huruf + " dengan Predikat " + predikat);
    }
}
