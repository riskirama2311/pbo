public class MahasiswaBaru extends Mahasiswa {
    String asalSekolah;

    public boolean ikutOspek() {
        if (semester == 1) {
            System.out.println("Mahasiswa Harus Mengikuti Ospek, Karena Mahasiswa Masih Semester 1");
            return true;
        } else {
            System.out.println("Mahasiswa Tidak mengikuti Ospek, Karena Mahasiswa Sudah di Atas Semester 1");
            return false;
        }
    }

    @Override
    public void infoMahasiswa() {
        System.out.println("Inputkan Asal Sekolah :" + this.asalSekolah);
        super.infoMahasiswa();
    }
}
